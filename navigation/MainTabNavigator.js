import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../src/constants/Colors';

import HomeScreen from '../src/screens/HomeScreen';
import ItemsScreen from '../src/screens/ItemsScreen';
import LinksScreen from '../src/screens/LinksScreen';
import SettingsScreen from '../src/screens/SettingsScreen';
import ThingScreen from '../src/screens/ThingScreen';

export default TabNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Items: {
      screen: ItemsScreen,
    },
    Thing: {
      screen: ThingScreen,
    },
    Links: {
      screen: LinksScreen,
    },
    Settings: {
      screen: SettingsScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Home':
            iconName = Platform.OS === 'ios'
              ? `ios-information-circle${focused ? '' : '-outline'}`
              : 'md-information-circle';
            break;
          case 'Thing':
            iconName = Platform.OS === 'ios'
              ? `ios-add-circle${true ? '' : '-outline'}`
              : 'md-add-circle';
            break;
          case 'Items':
            iconName = Platform.OS === 'ios'
              ? `ios-list${focused ? '' : '-outline'}`
              : 'md-list';
            break;
          case 'Links':
            iconName = Platform.OS === 'ios'
              ? `ios-link${focused ? '' : '-outline'}`
              : 'md-link';
            break;
          case 'Settings':
            iconName = Platform.OS === 'ios'
              ? `ios-options${focused ? '' : '-outline'}`
              : 'md-options';
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

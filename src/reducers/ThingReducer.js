import {
  SAVE_THING_SUCCESS,
  SAVE_THING_FAIL,
  DELETE_THING,
}
from '../actions/types';

const INITIAL_STATE = {
  data: { id: null, name: ''},
  result: null,
  message: '',
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SAVE_THING_SUCCESS:
      return { ...state, message: '' }
    case SAVE_THING_FAIL:
      return { ...state, message: action.payload.message  }
    default:
      return state;
  }
}

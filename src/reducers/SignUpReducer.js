import {
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
  SIGNUP_USER,
}
from '../actions/types';

const INITIAL_STATE= {
  result: null,
  message: ''
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SIGNUP_USER_SUCCESS:
      return { ...state, message: action.payload, result: true};
    case SIGNUP_USER_FAIL:
      return { ...state, message: JSON.stringify(action.payload), result: false };
    default:
      return state;
  }
}

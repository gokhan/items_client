import {
  SIGNIN_USER,
  SIGNIN_USER_FAIL,
  SIGNIN_USER_SUCCESS,
  LOGOUT,
}
from '../actions/types';

const INITIAL_STATE= {
  authenticated: false,
  message: '',
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SIGNIN_USER:
      return { ...state }
    case SIGNIN_USER_SUCCESS:
      return { ...state, authenticated: true, message: 'Login succeeded'}
    case SIGNIN_USER_FAIL:
      return { ...state, authenticated: false, message: 'Login failed!!!' }
    case LOGOUT:
      return { ...state, authenticated: false, message: '' };
    default:
      return state;
  }
}

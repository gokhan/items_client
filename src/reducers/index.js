import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import SignUpReducer from './SignUpReducer';
import ThingReducer from './ThingReducer';

export default combineReducers({
  auth: AuthReducer,
  thing: ThingReducer,
  signUp: SignUpReducer,
})

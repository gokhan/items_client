import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../constants/App';

import {
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNIN_USER_FAIL,
  LOGOUT
} from './types';


export const signInUser =  ({ email, password }) => {
  return async (dispatch) => {
    dispatch({ type: SIGNIN_USER });
    try{
      let response = await axios.post(`${API_URL}/api/authenticate`, { auth: { email, password } });
      await AsyncStorage.setItem('token', response.data.jwt);
      loginUserSuccess(dispatch, response.data.jwt);
    }
    catch(err) {
      loginUserFail(dispatch);
    }
  };
};

const loginUserFail = (dispatch) => {
  dispatch({
    type: SIGNIN_USER_FAIL
  });
}

const loginUserSuccess = (dispatch, token) => {
  axios.defaults.headers.common['Authorization'] = token;
  dispatch({
    type: SIGNIN_USER_SUCCESS,
    payload: token
  });
}


export const logout = (dispatch) => {
  return async (dispatch) => {
    dispatch({
      type: LOGOUT
    });
  };
}

export const SIGNIN_USER = 'signin_user';
export const SIGNIN_USER_SUCCESS = 'signin_user_success';
export const SIGNIN_USER_FAIL = 'signin_user_fail';
export const LOGOUT = 'logout';

export const SAVE_THING = 'save-thing';
export const DELETE_THING = 'delete-thing';
export const SAVE_THING_SUCCESS = 'save_thing_success';
export const SAVE_THING_FAIL = 'save_thing_fail';

export const SIGNUP_USER_SUCCESS = 'signup_user_success';
export const SIGNUP_USER_FAIL = 'signup_user_fail';

import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../constants/App';

import {
  SAVE_THING,
  SAVE_THING_SUCCESS,
  SAVE_THING_FAIL,
  DELETE_THING,
} from './types';


export const saveThing =  ({ id, name }) => {
  return async (dispatch) => {
      try{
        let token =  await AsyncStorage.getItem('token');
        axios.defaults.headers.common['Authorization'] = token;

        let response = await axios.post(`${API_URL}/api/items`, {
          item: { name }
        });

        dispatch({ type: SAVE_THING_SUCCESS });
      }
      catch(err) {
        dispatch({ type: SAVE_THING_FAIL, payload: { message: 'Saving failed because TODO' } });
      }
    };
};

import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../constants/App';

import {
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
} from './types';


export const signUpUser =  ({ email, password }) => {
  return async (dispatch) => {
    let response = await axios.post(`${API_URL}/api/users`, { user: { email, password } });
    const { meta } = response.data;
    if(response.data.error){
      dispatch({ type: SIGNUP_USER_FAIL, payload: response.data.error });
    }else{
      dispatch({ type: SIGNUP_USER_SUCCESS, payload: 'You have successfully signed up.' });
    }
  };
};

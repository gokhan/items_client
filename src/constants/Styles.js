export default {
  headerText: {
    marginLeft: 18
  },
  buttonStyle: {
    marginTop:10,
    marginLeft:5,
    marginRight:5,
    backgroundColor: '#0066ff',
  },
  dividerStyle: {
    marginTop: 30,
    marginBottom: 15,
  }
};

import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';
import Styles from '../../constants/Styles';
import { saveThing } from '../../actions';

class ThingEdit extends React.Component {
  state = {data: {id: null, name: '', tag_list: []} };
  const tags = ['restaurant', 'cafe', 'movie', 'book'];

  onButtonPress() {
    const { name } = this.state.data;
    this.props.saveThing({ name });
  }

  renderTags(){
    return tags.map.each(tag, function(tag){
      
    })
  }

  render() {
    return(
      <View>
        <FormLabel>Name</FormLabel>
        <FormInput
          placeholder='Please name your entry...'
          value={this.state.name}
          onChangeText={ name => this.setState({ data: { ...this.state.data, name } }) }
          />
        {this.renderTags()}
        <Button
          title='SAVE'
          onPress={this.onButtonPress.bind(this)}
          style={Styles.buttonStyle} small raised
          buttonStyle={{backgroundColor: '#2096f3'}}
        />
      </View>
    )
  }
}

const mapStateToProps = ({ thing }) => {
  const { message } = thing;
  return { message };
}

export default connect(mapStateToProps, { saveThing })(ThingEdit);

import React from 'react';
import { View, Text } from 'react-native';

const Alert = (props) => {
  return(
    <View style={styles.containerStyle}>
      <Text>{props.children}</Text>
    </View>
  )
}

const styles = {
  containerStyle: {
    backgroundColor: '#ccc',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  }
}

export { Alert };

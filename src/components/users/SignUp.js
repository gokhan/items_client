import React from 'react';
import { connect } from 'react-redux';
import { Component, View } from 'react-native';
import { Text, FormLabel, FormInput, Button } from 'react-native-elements';
import { Alert } from '../common';
import Styles from '../../constants/Styles';
import { signUpUser } from '../../actions';

class SignUp extends React.Component {
  state = { name: '', password: ''}

  onButtonPress() {
    const { email, password } = this.state;
    this.props.signUpUser( { email, password });
  }

  render() {
    return(
      <View>
        <Text h2 style={Styles.headerText}>Sign up</Text>
        <FormLabel>Email</FormLabel>
        <FormInput
          placeholder='Please enter your email...'
          value={this.state.email}
          onChangeText={ email => this.setState({ email })}
          />

        <FormLabel>Password</FormLabel>
        <FormInput
          placeholder='Please enter your password...'
          value={this.state.password}
          onChangeText={ password => this.setState({ password })}
        />
        <Button title='SIGN UP'
          style={Styles.buttonStyle} small raised
          onPress={this.onButtonPress.bind(this)}
          />
          <Alert>{this.props.message}</Alert>
      </View>
    );
  }
}

const mapStateToProps = ({ signUp }) => {
  const { message } = signUp;
  return { message };
}

export default connect(mapStateToProps, { signUpUser})(SignUp);

import React from 'react';
import { connect } from 'react-redux';
import { Component, View } from 'react-native';
import { Text, FormLabel, FormInput, Button } from 'react-native-elements';
import Styles from '../../constants/Styles';
import { signInUser } from '../../actions';

class SignIn extends React.Component {
  state = { email: 'user@example.com', password: 'user'};

  onButtonPress(){
    const { email, password } = this.state;
    this.props.signInUser({ email, password });
  }

  render() {
    return(
      <View>
        <Text h2 style={Styles.headerText}>Sign in</Text>
        <FormLabel>Email</FormLabel>
        <FormInput
          placeholder='Please enter your email...'
          value={this.state.email}
          onChangeText={email => this.setState({email})}
        />

        <FormLabel>Password</FormLabel>
        <FormInput
          placeholder='Please enter your password...'
          value={this.state.password}
          onChangeText={password => this.setState({password})}
        />
        <Button title='SIGN IN'
          onPress={this.onButtonPress.bind(this)}
          style={Styles.buttonStyle} small raised
          buttonStyle={{backgroundColor: '#2096f3'}}/>
        <Text>{this.props.message}</Text>
      </View>
    );
  }
}


const mapStateToProps = ( { auth } ) => {
  const { authenticated, message } = auth
  return { authenticated, message };
}

export default connect(mapStateToProps, {signInUser})(SignIn);

import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  FlatList,
  TouchableHighlight,
  Platform,

} from 'react-native';
import { List, ListItem, Icon, } from 'react-native-elements';
import data from '../../data/items.json';

export default class ItemList extends React.Component {
  render() {
    return(
      <ScrollView
        style={styles.container}>
        <List style={styles.list}>
          {
            data.map((l, i) => (
              <ListItem
                component={TouchableHighlight}
                key={l.id}
                title={l.name}
                leftIcon={{name: l.icon}}
                onPress={this._onPressListItem}
              />
            ))
          }
          </List>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  list: {
    marginTop: 0,
    paddingTop: 0
  },
});

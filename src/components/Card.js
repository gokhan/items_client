import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import { Icon } from 'react-native-elements';
export default class Card extends React.Component {
  render() {
    return(
      <View
        style={styles.container}>
        <View style={styles.headerContainer}>
          <Icon
            name='local-movies'
            size={70}
            color='gray'
            />

          <View style={{flexDirection: 'column', marginTop: 7}}>
            <Text style={{flexWrap: "wrap"}}>
              <Text style={styles.bold}>Sunil Choudhary</Text> shared The Thinking Great Apes to the group: Atheist Republic.</Text>
            <Text style={{color: 'gray', fontSize: 13}}>15 min</Text>
          </View>
        </View>
        <Text style={styles.description}>This is our first card description</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10,
    borderWidth: 1,
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  bold: {
    fontWeight: 'bold',
    color: '#2e78b7'
  },
  description: {
    paddingLeft: 11,
  }
});

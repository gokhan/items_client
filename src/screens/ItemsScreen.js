import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  FlatList,
  TouchableHighlight,
  Platform,

} from 'react-native';
import { List, ListItem, Icon, } from 'react-native-elements';
import data from '../../data/items.json';
import ItemList from '../components/ItemList';

export default class ItemsScreen extends React.Component {
  state = {showNewSection: false};

  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state;
    return{
      title: 'Items',
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({ _onPressNew: this._onPressNew });
  }

  render() {
    return(
        <View style={styles.container}>
          <ItemList />
          {this._showNewSection()}
        </View>
    );
  }

  _showNewSection = () => {
    if(this.state.showNewSection === false){
      return null;
    }

    return(
      <View style={styles.tabBarInfoContainer}>
        <Icon
          name='book'
          size={25}
          />
        <Icon
          name='local-movies'
          color='#999'
          size={25}
          />
        <Icon
          name='map'
          color='blue'
          size={25}
        />
      </View>
    );
  }

  _onPressNew = () => {
    console.log('Pressed');
    this.setState({showNewSection: true})
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 5,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
});

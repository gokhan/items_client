import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';
import Styles from '../constants/Styles';
import { logout } from '../actions';

class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Links',
  };

  onButtonPress(){
    this.props.logout();
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {/* Go ahead and delete ExpoLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */}
        <ExpoLinksView />
        <Button title='LOGOUT'
          onPress={this.onButtonPress.bind(this)}
          style={Styles.buttonStyle} small raised
          buttonStyle={{backgroundColor: '#cc3300'}}/>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

export default connect(null, {logout})(LinksScreen);

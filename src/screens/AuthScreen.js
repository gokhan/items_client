import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Divider } from 'react-native-elements';
import { WebBrowser } from 'expo';
import Styles from '../constants/Styles';

import { MonoText } from '../components/StyledText';
import Card from '../components/Card';
import SignUp from '../components/users/SignUp';
import SignIn from '../components/users/SignIn';

export default class AuthScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <SignUp />
          <Divider style={Styles.dividerStyle} />
          <SignIn />
        </ScrollView>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  contentContainer: {
    paddingTop: 30,
  },
});

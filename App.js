import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { connect } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Main from './Main';

class App extends Component {

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default App;
